var config = require('./config.json');
var express = require('express');
var app = express();
var fs = require("fs");
var mysql = require('mysql');
var bodyParser = require("body-parser");
var moment = require('moment');
var check = require('validator');

var mongoose = require('mongoose');
/* var Schema = mongoose.Schema; */

var jwt = require('jsonwebtoken');

var common = require('./common');
var loggedUser = {};
var token = '';

var con = mysql.createConnection({
  host: config.dbHost,
  user: config.dbUser,
  password: config.dbPassword,
  database: config.dbName
});

con.connect(function (err) {
  if (err) throw err;
  // console.log("DB Connected!");
});

con.on('error', function (err) {
  // console.log('MYSQL Error', err.code);
});

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://192.168.0.3:4200');
  // res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,x-access-token');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

app.set('superSecret', config.secret); // secret variable

app.post('/signin', function (req, res) {
  // // console.log('Login route', req.body);
  var email = req.body.email;
  var password = req.body.password;
  var sql = "select u.name, u.email, u.uid, u.updated_datetime, u.created_datetime   from users u where u.`email`='" + email + "' AND u.`password`='" + password + "'";
  // // console.log('sql', sql);
  con.query(sql, function (err, result) {
    if (err) throw err;
    var response = {};
    if (result.length > 0) {
      const payload = JSON.stringify(result[0]);
      // console.log(payload);
      /* var token = jwt.sign(payload, app.get('superSecret'), {
        expiresInMinutes: 1440 // expires in 24 hours
      }); */
      var token = jwt.sign({
        data: payload
      }, app.get('superSecret'), { expiresIn: 60 * 60 * 24 }); // 60 * 60 * 24 = 1 day

      // console.log('generated token', token);
      response['data'] = result[0];
      response['token'] = token;
      response['success'] = true;
      response['message'] = "Logged Successfully";
      res.json(response);
    } else {
      response['error'] = "";
      response['success'] = false;
      response['message'] = "Incorrect login details";
      res.json(response);
    }
  });
})

app.post('/signup', function (req, res) {
  // // console.log('Login route', req.body);
  // var postData = common.getSanitize(req.body);
  var postData = req.body;
  var response = {};
  // // console.log(postData);
  var isDataValid = true;
  if (!check.isEmail(postData.email)) {
    isDataValid = false;
    response['error'] = [{ 'email': "Email is invalid" }];
    response['success'] = false;
    response['message'] = "Email is invalid";
    res.json(response);
  }
  if (postData.password !== postData['re-password']) {
    isDataValid = false;
    response['error'] = [{ 'password': "doesn't match", 're-password': "doesn't match" }];
    response['success'] = false;
    response['message'] = "Password doesn't match";
    res.json(response);
  }
  if (isDataValid) {
    // // console.log('Valid Data');
    var sql = "select email from users where email='" + postData.email + "'";
    // // console.log('sql', sql);
    con.query(sql, function (err, result) {
      if (err) throw err;
      // var response = {};
      // // console.log('Email result', result);
      if (result.length > 0) {
        response['error'] = [{ 'email': "Email is already registered" }];
        response['success'] = false;
        response['message'] = "Email is already registered";
        res.json(response);
      } else {
        // add new user
        var created_datetime = moment().utc().utcOffset(-4).format('YYYY-MM-DD HH:mm:ss');
        var sql = "INSERT INTO `users` (name, email, password , status,created_datetime) VALUES ('" + postData.name + "', '" + postData.email + "', '" + postData.password + "', '1', '" + created_datetime + "')";
        // // console.log('insert sql', sql);
        con.query(sql, function (err, result) {
          if (err) {
            // throw err;
            response['error'] = err;
            response['success'] = false;
            response['message'] = "Something went wrong.";
            res.json(response);
          }
          var response = {};
          if (result.insertId > 0) {
            response['data'] = result.insertId;
            response['success'] = true;
            response['message'] = "User Added Successfully";
            res.json(response);
          } else {
            response['error'] = "";
            response['success'] = false;
            response['message'] = "Something went wrong.";
            res.json(response);
          }
        });
      }
    });
  }
})
var apiRoutes = express.Router(); 
apiRoutes.use(function (req, res, next) {

  // check header or url parameters or post parameters for token
  token = req.body.token || req.query.token || req.headers['x-access-token'];
  // decode token
  if (token) {

    // verifies secret and checks exp
    jwt.verify(token, app.get('superSecret'), function (err, decoded) {
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' });
      } else {
        // if everything is good, save to request for use in other routes
        loggedUser = req.decoded = decoded;
        loggedUser = JSON.parse(loggedUser.data);
        next();
      }
    });

  } else {

    // if there is no token
    // return an error
    return res.status(403).send({
      success: false,
      message: 'No token provided.'
    });

  }
});
app.use('/api', apiRoutes); 

apiRoutes.get('/menu', function (req, res) {
  // console.log('user, token', loggedUser, token);
  var sql = "select * from `menu` pm";
  // console.log('sql', sql);
  con.query(sql, function (err, result) {
    if (err) throw err;
    var response = {};
    if (result.length > 0) {
      response['data'] = result;
      response['success'] = true;
      response['message'] = "Menu Data";
      res.json(response);
    } else {
      response['error'] = "";
      response['success'] = false;
      response['message'] = "No Data Found";
      res.json(response);
    }
  });
})

apiRoutes.get('/product/list', function (req, res) {
  /*IF(uc.ucid IS NULL, 'notInCart', IF(uc.`status`='1', uc.ucid, 'notInCart')) ucid */
  var sql = "select " +
    "p.pid, p.product_name, p.product_sku, p.price,p.product_image , p.product_description, p.`status`, " +
    "pc.category_name, pc.category_description," +
    "pq.quantity, " +
    "IFNULL((SELECT ucid FROM user_cart uc WHERE uc.pid=p.pid AND uc.product_sku=p.product_sku AND uc.`status`=1 AND uc.uid='"+ loggedUser.uid +"'),'notInCart') ucid " +
    "from products p " +
    "LEFT JOIN product_category_relation pcr ON p.pid=pcr.pid " +
    "LEFT JOIN product_category pc ON pc.pcid=pcr.pcid " +
    "LEFT JOIN product_quantity pq ON pq.pid=p.pid ";
  // // console.log('sql1', sql);
  con.query(sql, function (err, result) {
    if (err) throw err;
    // // console.log("Result: ", result);
    // res.json(result);
    var response = {};
    response['data'] = result;
    response['success'] = true;
    response['message'] = "Creds Data";
    res.json(response);
  });
})

apiRoutes.post('/add-cart', function (req, res) {
  var postData = req.body;
  // console.log(postData);
  var sql = "select * from products where pid='" + postData.pid + "'";
  // // console.log('sql', sql);
  con.query(sql, function (err, result) {
    if (err) throw err;
    var response = {};
    // // console.log('Product result', result);

    if (result.length > 0) {
      var productData = result[0];
      var sql = "select uid from users where uid='" + postData.uid + "'";
      // // console.log('sql', sql);
      con.query(sql, function (err, result) {
        if (err) throw err;
        // var response = {};
        // // console.log('user result', result);

        if (result.length > 0) {
          var sql = "INSERT INTO `user_cart` (uid, pid, product_sku , price, quantity) VALUES ('" + postData.uid + "', '" + postData.pid + "', '" + productData.product_sku + "', '" + productData.price + "', '1')";
          // // console.log('sql', sql);
          con.query(sql, function (err, result) {
            if (err) throw err;
            // var response = {};

            if (err) {
              // throw err;
              response['error'] = err;
              response['success'] = false;
              response['message'] = "Something went wrong.";
              res.json(response);
            }
            if (result.insertId > 0) {
              response['data'] = result.insertId;
              response['success'] = true;
              response['message'] = "Product Added to Cart";
              res.json(response);
            } else {
              response['error'] = "";
              response['success'] = false;
              response['message'] = "Something went wrong.";
              res.json(response);
            }
          });
        } else {
          response['error'] = "";
          response['success'] = false;
          response['message'] = "Product not found.";
          res.json(response);
        }
      });
    } else {
      response['error'] = "";
      response['success'] = false;
      response['message'] = "User not found.";
      res.json(response);
    }
  });
})

apiRoutes.post('/cart/update/', function (req, res) {
  var postData = req.body;
  // // console.log(postData);
  var sql = "SELECT * FROM products p LEFT JOIN product_quantity pq ON p.pid=pq.pid where p.pid='" + postData.pid + "'";
  // // console.log('sql', sql);
  con.query(sql, function (err, result) {
    if (err) throw err;
    var response = {};
    // // console.log('Product result', result);

    if (result.length > 0) {
      var productData = result[0];
      // // console.log(productData, postData.quantity);
      if (productData.quantity >= postData.quantity) {
        var sql = "select uid from users where uid='" + postData.uid + "'";
        // // console.log('sql', sql);
        con.query(sql, function (err, result) {
          if (err) throw err;
          // var response = {};
          // // console.log('user result', result);

          if (result.length > 0) {
            var sql = "UPDATE user_cart uc SET uc.`quantity`='" + postData.quantity + "' where uc.ucid='" + postData.ucid + "' AND uc.pid='" + postData.pid + "'";
            // // console.log('sql', sql);
            con.query(sql, function (err, result) {
              if (err) throw err;
              // var response = {};
              if (err) {
                // throw err;
                response['error'] = err;
                response['success'] = false;
                response['message'] = "1Something went wrong.";
                res.json(response);
              }
              if (result.affectedRows > 0) {
                response['data'] = result.affectedRows;
                response['success'] = true;
                response['message'] = "Product Added to Cart";
                res.json(response);
              } else {
                response['error'] = "";
                response['success'] = false;
                response['message'] = "2Something went wrong.";
                res.json(response);
              }
            });
          } else {
            response['error'] = "";
            response['success'] = false;
            response['message'] = "Product not found.";
            res.json(response);
          }
        });
      } else {
        response['error'] = "";
        response['success'] = false;
        response['message'] = "Only " + productData.quantity + " left!";
        res.json(response);
      }
    } else {
      response['error'] = "";
      response['success'] = false;
      response['message'] = "User not found.";
      res.json(response);
    }
  });
})

apiRoutes.get('/cart/get/:uid', function (req, res) {
  // console.log(req.params.uid);
  var sql = "select " +
    "uc.quantity, uc.ucid, " +
    "p.pid, p.product_name, p.product_image, p.product_sku, p.price, " +
    "pq.quantity maxQuantity  " +
    "from user_cart uc " +
    "LEFT JOIN products p ON uc.pid=p.pid AND uc.product_sku=p.product_sku " +
    "LEFT JOIN product_quantity pq ON pq.pid=p.pid " +
    "where p.`status` =1 AND uc.`status` =1 " +
    "AND uc.uid=" + req.params.uid;       /**Should be changed to session user */
  // // console.log('sql', sql);
  con.query(sql, function (err, result) {
    if (err) throw err;
    var response = {};
    response['data'] = result;
    response['success'] = true;
    response['message'] = "Creds Data";
    res.json(response);
  });
})

apiRoutes.get('/cart/delete/:ucid/:pid', function (req, res) {
  // // console.log(req.params);
  var sql = "select uc.ucid FROM user_cart uc WHERE uc.ucid=" + req.params.ucid + " AND uc.pid=" + req.params.pid;
  // console.log('sql', sql);
  con.query(sql, function (err, result) {
    if (err) throw err;
    // console.log("Result: ", result);
    // res.json(result);
    var response = {};
    if (result.length > 0) {
      var sql = "UPDATE user_cart uc SET uc.`status`=0 where uc.ucid=" + req.params.ucid + " AND uc.pid=" + req.params.pid;
      // console.log('sql', sql);
      con.query(sql, function (err, result) {
        if (err) throw err;
        var response = {};
        if (err) {
          // throw err;
          response['error'] = err;
          response['success'] = false;
          response['message'] = "Something went wrong.";
          res.json(response);
        }
        if (result.affectedRows > 0) {
          response['data'] = result.affectedRows;
          response['success'] = true;
          response['message'] = "Product Removed from Cart";
          res.json(response);
        } else {
          response['error'] = "";
          response['success'] = false;
          response['message'] = "Something went wrong.";
          res.json(response);
        }
      });
    } else {
      response['error'] = "";
      response['success'] = false;
      response['message'] = "No Data found.";
      res.json(response);
    }
  });
})

var server = app.listen(8081, '192.168.0.3', function () {

  var host = server.address().address
  var port = server.address().port || 8081;

  console.log("Example app listening at http://%s:%s", host, port)

});