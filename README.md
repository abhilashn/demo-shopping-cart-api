*Shopping App API*
A sample APi with Node, eclipse

Requirements
- Node
- Express
- MySQl/MariaDB

Steps
- Install latest NodeJS
- Install MySQl/MariaDB
- Clone repository
- Change path to project directory
- npm install
- Dump DB from the *.sql file
- Run node server using node server.js

Contact
Abhilash